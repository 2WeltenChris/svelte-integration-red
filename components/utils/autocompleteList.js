// This is a hand-generated list of completions for the core nodes (based on the node help html).
// current state: Node-RED 4.0.8
export const msgCompletions = [
  { value: 'payload' },
  { value: 'topic', source: ['mqtt', 'inject', 'rbe'] },
  { value: 'action', source: ['mqtt'] },
  { value: 'complete', source: ['join'] },
  { value: 'contentType', source: ['mqtt'] },
  { value: 'cookies', source: ['http request', 'http response'] },
  { value: 'correlationData', source: ['mqtt'] },
  { value: 'delay', source: ['delay', 'trigger'] },
  { value: 'encoding', source: ['file'] },
  { value: 'error', source: ['catch'] },
  { value: 'error.message', source: ['catch'] },
  { value: 'error.source', source: ['catch'] },
  { value: 'error.source.id', source: ['catch'] },
  { value: 'error.source.type', source: ['catch'] },
  { value: 'error.source.name', source: ['catch'] },
  { value: 'filename', source: ['file', 'file in'] },
  { value: 'flush', source: ['delay'] },
  { value: 'followRedirects', source: ['http request'] },
  { value: 'headers', source: ['http response', 'http request'] },
  { value: 'host', source: ['tcp request', 'http request'] },
  { value: 'ip', source: ['udp out'] },
  { value: 'kill', source: ['exec'] },
  { value: 'messageExpiryInterval', source: ['mqtt'] },
  { value: 'method', source: ['http request'] },
  { value: 'options', source: ['xml'] },
  { value: 'parts', source: ['split', 'join', 'batch', 'sort'] },
  { value: 'pid', source: ['exec'] },
  { value: 'port', source: ['tcp request', ' udp out'] },
  { value: 'qos', source: ['mqtt'] },
  { value: 'rate', source: ['delay'] },
  { value: 'rejectUnauthorized', source: ['http request'] },
  { value: 'req', source: ['http in'] },
  { value: 'req.body', source: ['http in'] },
  { value: 'req.headers', source: ['http in'] },
  { value: 'req.query', source: ['http in'] },
  { value: 'req.params', source: ['http in'] },
  { value: 'req.cookies', source: ['http in'] },
  { value: 'req.files', source: ['http in'] },
  { value: 'requestTimeout', source: ['http request'] },
  { value: 'reset', source: ['delay', 'trigger', 'join', 'rbe'] },
  { value: 'responseCookies', source: ['http request'] },
  { value: 'responseTopic', source: ['mqtt'] },
  { value: 'responseUrl', source: ['http request'] },
  { value: 'restartTimeout', source: ['join'] },
  { value: 'retain', source: ['mqtt'] },
  { value: 'schema', source: ['json'] },
  { value: 'select', source: ['html'] },
  { value: 'statusCode', source: ['http response', 'http request'] },
  { value: 'status', source: ['status'] },
  { value: 'status.text', source: ['status'] },
  { value: 'status.source', source: ['status'] },
  { value: 'status.source.type', source: ['status'] },
  { value: 'status.source.id', source: ['status'] },
  { value: 'status.source.name', source: ['status'] },
  { value: 'target', source: ['link call'] },
  { value: 'template', source: ['template'] },
  { value: 'toFront', source: ['delay'] },
  { value: 'url', source: ['http request'] },
  { value: 'userProperties', source: ['mqtt'] },
  { value: '_session', source: ['websocket out', 'tcp out'] }
]
