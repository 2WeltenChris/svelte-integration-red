/* global localStorage, */

// Translation:
// current way with Node-RED tools:
// - an i18nOptions object for the node where we can set a prefix if the Node-Red node is not in the main folder.
// - to translate in a SIR component object just set the node property and set the path within the json file, e.g.: label:{ label.name }.
// - instead of giving the node property you can set an i18n property with the path to the local folder, e.g.: 'test-node/second-node:' or if in the main folder: 'second-node:'
//
// a different approach to the code below is to use the offical way like:
// - direct i18n in html <span data-i18n="test-node/second-node:second-node.label.name"></span>
// - setting to placeholder prop <input type="text" data-i18n="[placeholder]myNode.placeholder.foo">
//
// Sidenote: This translation is only working for custom nodes. Translations for the sir components are within their files.
// This must be done that way, as we must ensure translation, whether SIR is installed or not. Also it seems that Node-Red only initialize translation files
// for nodes registered in the package.json.
// Addition: Since Node-Red 2.0.3 we can't import i18next as this will break all other translations.

export const preferredLanguage = localStorage.getItem('editor-language') || navigator.language || 'en-US'

export const i18nTranslate = (RED, node = {}, i18nDOM = true, translateThis) => {
  let result = translateThis
  // i18n can be a string for the local folder path (which we can't have without node) or boolean false if we don't want to translate this one
  if (i18nDOM === false || i18nDOM.toString().trim() === 'false') {
    return result
  }

  let path = ''
  if (typeof i18nDOM === 'string') {
    path = i18nDOM
  } else if (node) {
    path = node.type
    let folder = node?._def?.i18nOptions?.folder
    if (folder) {
      if (!folder.endsWith('/')) {
        folder += '/'
      }
      path = folder + path
    }
  }
  if (path) {
    if (!path.endsWith(':')) path += ':'
    result = RED._(path + translateThis)
    // RED._() will replace colon with dot, so we have to check if result is like that pattern
    const translateColonReplacedWithDot = translateThis.replaceAll(':', '.')
    if (result === path + translateThis || result === path + translateColonReplacedWithDot || result === translateColonReplacedWithDot) {
      // couldn't find translation
      result = translateThis
    }
  }
  return result
}
