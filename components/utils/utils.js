export const getBooleanFrom = (property) => {
  return (typeof property === 'boolean')
    ? property
    : property?.toString()?.toLowerCase() === 'true'
      ? true
      : !!property
}

export const getId = (prop = null) => {
  let id = prop
  if (!id) {
    id = (Date.now() + Math.floor(Math.random() * Number.MAX_SAFE_INTEGER)).toString()
  }
  return id
}

export const getFadeDuration = (fading) => {
  let fadeDuration = 800
  // do not use isNaN with boolean!
  if (typeof fading === 'number' || (typeof fading === 'string' && fading !== 'true' && fading !== 'false')) {
    fadeDuration = Number(fading)
  } else {
    fading = getBooleanFrom(fading)
    if (!fading) fadeDuration = 0
  }
  return fadeDuration
}

const setInternalValue = (internal, nodeProp, value) => {
  // value must not be undefined, as that can result in strange behaviour but can have other falsy value types (checkbox = false, number = 0)
  if (internal.updateNode && typeof nodeProp !== 'undefined') {
    return nodeProp
  } else if (typeof value !== 'undefined') {
    return value
  } else {
    return ''
  }
}

export const initInternal = (nodeProp, value) => {
  const internal = {
    init: true,
    isError: false,
    updateNode: typeof nodeProp !== 'undefined',
    valueHasChanged: false
  }
  // on init set the nodeProp.value else we have the whole node property (with label etc.)
  internal.value = setInternalValue(internal, nodeProp?.value, value)
  internal.oldValue = internal.value
  return internal
}

export const getNewInternal = (nodeProp, value, internal, validateFunction, error, node) => {
  internal.valueHasChanged = internal.updateNode ? nodeProp !== internal.value : internal.value !== value
  if (internal.valueHasChanged) {
    if (internal.value === internal.oldValue) {
      internal.value = setInternalValue(internal, nodeProp, value)
    }
    internal.oldValue = internal.value
  }
  if (validateFunction) {
    // add node to validate function to have access to node properties while validating when input field changes
    // use call function to ensure using "this" with the node value within the editor as well as if NR wants to validate
    const result = validateFunction.call(node, internal.value)
    if (typeof result === 'string') {
      internal.error = result
    } else {
      internal.error = !result
    }
  } else {
    internal.error = error
  }
  return internal
}

// escape "unallowed" jQuery chars
export const idSanitizer = (id) => id.replace(/(:|\.|\[|\]|,|=|@)/g, '\\$1')
