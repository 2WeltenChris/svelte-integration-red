module.exports = function (RED) {
  function TestNode (config) {
    RED.nodes.createNode(this, config)
    config = require('./test-node-update').serverUpdate(config)
    const node = this
    node.name = config.name

    node.on('input', function (msg, send, done) {
      msg.payload = {
        content: RED.util.evaluateNodeProperty(config.content, config.contentType, node, msg),
        content2: RED.util.evaluateNodeProperty(config.content2, config.contentType2, node, msg),
        username: node.credentials.username,
        password: node.credentials.password
      }
      send(msg)
      done()
    })
  }
  RED.nodes.registerType('test-node', TestNode, {
    credentials: {
      username: { type: 'text' },
      password: { type: 'password' }
    }
  })
}
