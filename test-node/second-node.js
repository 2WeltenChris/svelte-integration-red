module.exports = function (RED) {
  function SecondNode (config) {
    RED.nodes.createNode(this, config)
    const node = this
    node.name = config.name
  }
  RED.nodes.registerType('second-node', SecondNode)
}
