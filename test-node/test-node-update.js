const addLegacyProperties = () => {
  const legacyProps = []
  legacyProps.push('myProp')
  return legacyProps
}

const clientUpdate = (version, node) => {
  // v = {major: 0, minor: 1, patch: 1, tag: 'beta-1', string: '0.1.1-beta-1'}
  const { major, minor, patch } = version

  if (major <= 1) {
    // v.1.x.x
    if (minor <= 1) {
      // v.1.1.x
      if (patch < 1) {
        // v.1.1.1
        node.name = 'Enter a name'
      }
      if (patch < 2) {
        // v.1.1.2
        node.name = 'Enter a really good name'
      }
    }
  }
  return node
}

const serverUpdate = (config) => {
  // TODO find a good way to put the version parse automatically here...
  // const currentVersion = config._version
  return config
}

module.exports = {
  addLegacyProperties,
  clientUpdate,
  serverUpdate
}
