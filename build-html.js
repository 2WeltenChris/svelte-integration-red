const fs = require('fs')
const path = require('path')
const rollup = require('rollup')
const svelte = require('rollup-plugin-svelte')
const { nodeResolve } = require('@rollup/plugin-node-resolve')
const commonjs = require('@rollup/plugin-commonjs')
const { minify } = require('html-minifier-terser')

// ("use strict" mode needed for Safari leads to problem with e.g. "update" function for svelte store)
const createHtmlNode = (clientUpdatesCode, componentName, nodeType, latestVersion, code, documentationType, documentation) => `
<script type='text/javascript'>
  "use strict"
  let component_${componentName}
  {
    ${clientUpdatesCode}
    const sir_render = function (node, options) {
      try {
        if (typeof node !== 'object') {
          return
        }
        let minWidth = '400px'
        if (options) {
          if (options.minWidth) minWidth = options.minWidth
        }
        if (!node.__clone) {
          node.__clone = window.$.extend(true, {}, node)
        }
        component_${componentName} = new ${componentName}({
          target: document.getElementById('${nodeType}-svelte-container'),
          props: { node: node.__clone }
        })
        document.getElementById('${nodeType}-svelte-container').style.width = minWidth
        const nodeIsSidebarTab = !!node.onchange
        if (!nodeIsSidebarTab) {
          const orgResize = node._def.oneditresize
          node._def.oneditresize = function (size) {
            document.getElementById('${nodeType}-svelte-container').style.width = 'auto'
            if (orgResize) orgResize(size)
            node._def.oneditresize = orgResize
          }
        }
      } catch (e) {
          console.log(e)
      }
    }
    const sir_update = function (node, options = {}) {
      if (node.__clone) {
        const clone = node.__clone
        delete node.__clone
        const defaultKeys = Object.keys(node._def.defaults)
        for (const key of Object.keys(clone)) {
          if (defaultKeys.indexOf(key) === -1) {
            delete clone[key]
          }
        }
        // update config node sidebar
        let updateConfigSidebar = false
        for (const key of defaultKeys) {
          // is config node and has been changed?
          if (node._def.defaults[key].type && node[key] !== clone[key]) {
            updateConfigSidebar = true
            // config has changed, add to or remove from config node and refresh sidebar
            // The official RED.nodes.updateConfigNodeUsers(configNode) seems to have no effect.
            const oldConfig = RED.nodes.node(node[key])
            const newConfig = RED.nodes.node(clone[key])
            // Remove from old config node
            if (oldConfig) {
              oldConfig.users = oldConfig.users.filter(userNode => userNode.id !== node.id)
            }
            // add to new config node
            if (newConfig && !newConfig.users?.find(userNode => userNode.id === node.id)) {
              newConfig.users.push(RED.nodes.node(node.id))
            }
          }
        }
        if (updateConfigSidebar) {
          RED.sidebar.config.refresh()
        }

        Object.assign(node, clone)
      }

      // delay $destroy as NR takes some values directly from the input field (e.g. credential fields in config nodes)
      // and ignores that sir_update set them later into the node.credentials
      if (!options.isStartupUpdate) {
        setTimeout(() => component_${componentName}.$destroy(), 250) // 250ms like RED.tray.close()
      }
    }
    const sir_revert = function (node) {
      delete node.__clone
      // nothing to save -> can be destroyed now
      component_${componentName}.$destroy()
    }
    const sir_addCurrentNodeVersion = function (node) {
      node._version = '${latestVersion}'
    }
    ${code}
  }
</script>
<script type="text/x-red" data-template-name="${nodeType}">
  <div id='${nodeType + '-svelte-container'}' class="svelte-integration-red-node-container"></div>
</script>
<script type="${documentationType}" data-help-name="${nodeType}">
    ${documentation}
</script>
`
const createUpdateCode = (componentName, nodeType, updateFilepath, latestVersion) => {
  let clientUpdatesCode = ''
  const { clientUpdate, addLegacyProperties } = require(updateFilepath)
  if (clientUpdate) {
    let clientUpdateFunction = ''
    if (clientUpdate.toString().trim().startsWith('function')) {
      clientUpdateFunction = clientUpdate.replace('function', `function sir_${componentName}_update`)
    } else {
      clientUpdateFunction = `const sir_${componentName}_update = ` + clientUpdate
    }
    clientUpdatesCode = `
  function createVersionObject (v) {
    const parse = (str = '0.0.0') => {
      const versionArray = str.split('.')
      const major = versionArray.shift() || 0
      const minor = versionArray.shift() || 0
      let patch = versionArray.join('.') || '0'
      let tag = ''
      if (patch.includes('-')) {
        const patchAndTag = patch.split('-')
        patch = patchAndTag.shift()
        tag = patchAndTag.shift() 
      }
      const parsedVersion = {
        major: Number(major),
        minor: Number(minor),
        patch: Number(patch),
        tag,
        string: str
      }
      parsedVersion.aggregated = (parsedVersion.major * 1000000) + (parsedVersion.minor * 1000) + (parsedVersion.patch)
      return parsedVersion
    }
    const version = parse(v)
    // add to call version.parse("1.2.3") in client update
    version.parse = parse
    return version
  }
  let sir_legacyProperties = []
  ${clientUpdateFunction}
  function doUpdateOnStart (msg) {
    if (RED.runtime.started) {
      let updateVersion = false
      let isDirty = false
        setTimeout(function () {
        const nodeCategory = RED.nodes.getType('${nodeType}').category
        // for reasons RED.nodes.getType('x').defaults removes legacy props on the way between adding them via addLegacyProperties and update here
        // the old value is kept in the node
        const defaultKeys = [...Object.keys(RED.nodes.getType('${nodeType}').defaults), ...sir_legacyProperties]
        const eachNodeFunction = nodeCategory === 'config' ? RED.nodes.eachConfig : RED.nodes.eachNode
        eachNodeFunction(node => {
          if (node.type === '${nodeType}' && node._version !== '${latestVersion}') {
            // update a clone (only defaults) and check if they are different after update
            const clone = window.$.extend(true, {}, node)

            for (const key of Object.keys(clone)) {
              if (defaultKeys.indexOf(key) === -1 && key !== 'id') {
                delete clone[key]
              }
            }
            const cloneAfterUpdate = sir_${componentName}_update(createVersionObject(node._version), window.$.extend(true, {}, clone))
            if (JSON.stringify(clone) !== JSON.stringify(cloneAfterUpdate)) {
              node.__clone = cloneAfterUpdate
              sir_update(node, { isStartupUpdate: true })
              // set later all nodes of this type to the latest version, regardless if something has changed for a specific node
              updateVersion = true
            }
          }
        })
        if (updateVersion) {
          const updateText = 'Updated node ${nodeType} to ${latestVersion}. Please check the node or deploy the automatic update changes.'
          RED.notify(updateText)
          console.log('[sir] ' + updateText)
          // update all nodes to the latest version
          eachNodeFunction(node => {
            if (node.type === '${nodeType}' && node._version !== '${latestVersion}') {
              node._version = '${latestVersion}'
              // remove legacy props from node as they are always added and not relevant after update anymore
              sir_legacyProperties.forEach(property => delete node[property])
              node.dirty = true
              node.changed = true
              isDirty = true
            }
          })
        }

        // remove legacy properties
        if (sir_legacyProperties?.length) {
          // remove from defaults
          const typeDefaults = RED.nodes.getType('${nodeType}').defaults
          sir_legacyProperties.forEach(legacyProperty => delete typeDefaults[legacyProperty])
          // remove from existing nodes
          RED.nodes.filterNodes({'type': 'kdbx'}).forEach(n => {
            sir_legacyProperties.forEach(legacyProperty => delete n[legacyProperty])
          })
        }

        if (isDirty) {
          RED.nodes.dirty(true)
        }
        RED.view.select("") // update UI
      }, 1000);
    } else {
      setTimeout(() => doUpdateOnStart(), 1000)
    }
  }
  doUpdateOnStart()`

    // add legacy properties is only useful if there is an update code, as there are no manipulations for existing nodes possible.
    if (addLegacyProperties) {
      let addLegacyPropertiesFunction = ''
      if (addLegacyProperties.toString().trim().startsWith('function')) {
        addLegacyPropertiesFunction = addLegacyProperties.replace('function', `function sir_${componentName}_addLegacyProperties`)
      } else {
        addLegacyPropertiesFunction = `const sir_${componentName}_addLegacyProperties = ` + addLegacyProperties
      }
      clientUpdatesCode += `
      ${addLegacyPropertiesFunction}
    // always add all legacy props and remove them after update check (Version 0.0.0 is for older versions)
    sir_legacyProperties = sir_${componentName}_addLegacyProperties('0.0.0')

  const sir_event_addLegacyProperties = (nodeType) => {
    if (nodeType === '${nodeType}') {
      const defaults = RED.nodes.getType('${nodeType}').defaults
      sir_legacyProperties.forEach(property => {
        defaults[property] = { value: '' }
      })
    }
    RED.events.off('registry:node-type-added', sir_event_addLegacyProperties)
  }
  RED.events.on('registry:node-type-added', sir_event_addLegacyProperties)
  `
    }
  }
  return clientUpdatesCode
}

let tempFile
async function build (baseDirOrFile) {
  // check if directory or file
  let baseDir = baseDirOrFile
  let filename = ''
  if (baseDirOrFile.endsWith('.svelte')) {
    const parsedPath = path.parse(baseDirOrFile)
    baseDir = parsedPath.dir
    filename = parsedPath.name
  }
  const packageJson = JSON.parse(fs.readFileSync(path.join(baseDir, 'package.json')))

  // Special case like theme with plugin which has the same name in node and plugin
  let nodes
  if (process.argv.find(a => a === '-onlyNodes')) {
    nodes = packageJson['node-red']?.nodes || {}
  } else if (process.argv.find(a => a === '-onlyPlugins')) {
    nodes = packageJson['node-red']?.plugins || {}
  } else {
    nodes = { ...packageJson['node-red']?.nodes, ...packageJson['node-red']?.plugins }
  }

  const latestVersion = packageJson.version
  let keys = Object.keys(nodes)
  if (filename) {
    // only handle this file
    keys = keys.filter(k => k === filename)
  }

  for (const node of keys) {
    const nodeJs = path.join(baseDir, nodes[node])
    const svelteFilePath = nodeJs.replace('.js', '.svelte')
    const nodeName = node
    if (fs.existsSync(svelteFilePath)) {
      console.log(`[sir] Found svelte-file "${svelteFilePath}" for node "${node}".`)
      // rewrite common functions to prevent name clashing
      let code = fs.readFileSync(svelteFilePath, 'utf8')
      code = code.replace(/<script.*context=("|')module("|')>/, `<script context="module">
        const render = sir_render
        const update = sir_update
        const revert = sir_revert
        const addCurrentNodeVersion = sir_addCurrentNodeVersion
      `)
      // rollup virtual file seems to have problems with <script /> parts
      // Current workaround: saving temp file and removing it later -> Warning: export component name will have the timestamp too.
      tempFile = svelteFilePath.replace('.svelte', '_' + Date.now() + '.svelte')
      fs.writeFileSync(tempFile, code)

      try {
        // Build page
        const result = await rollup.rollup({
          input: tempFile,
          plugins: [
            svelte({
              extensions: ['.svelte'],
              emitCss: false
            }),
            nodeResolve({
              browser: true,
              dedupe: importee => importee === 'svelte' || importee.startsWith('svelte/'),
              preferBuiltins: false
            }),
            commonjs()
          ],
          output: {
            format: 'cjs',
            exports: 'named',
            inlineDynamicImports: true
          }
        })

        const bundle = await result.generate({})
        // Create wrapper html
        const wrapperFile = nodeJs.replace('.js', '.html')
        const filename = nodeJs.replace('.js', '')
        let documentationType = 'text/html'
        let documentation = 'No documentation yet.'
        if (fs.existsSync(filename + '.doc.html')) {
          documentation = fs.readFileSync(filename + '.doc.html')
        } else if (fs.existsSync(filename + '.doc.md')) {
          documentation = fs.readFileSync(filename + '.doc.md')
          documentationType = 'text/markdown'
        }
        let code = bundle.output[0].code
        // const componentName = code.match(/export { (?<component>\S+) as default };/).groups.component
        // Remove the timestamp from the code name to prevent unnecessary file changes / commits (svelte creates export name with timestamp...)
        const componentNameWithTimestamp = code.match(/export { (?<component>\S+) as default };/).groups.component
        const componentNameArray = componentNameWithTimestamp.split('_')
        const timestamp = componentNameArray.pop()
        const componentName = componentNameArray.join('_')
        code = code.replaceAll(componentNameWithTimestamp, componentName)
        code = code.replaceAll('_' + timestamp + '.svelte', '.svelte') // svelte comments
        // It wouldn't work as a module as then the node would get registered too late
        code = code.replace(/export { \S+ as default };/, '')
        let clientUpdateCode = ''
        const updateNode = nodeJs.replace('.js', '-update.js')
        if (fs.existsSync(updateNode)) {
          const updateNodePath = path.resolve(updateNode)
          clientUpdateCode = createUpdateCode(componentName, nodeName, updateNodePath, latestVersion)
        }
        let htmlNode = createHtmlNode(clientUpdateCode, componentName, nodeName, latestVersion, code, documentationType, documentation)

        // do not minify code if flag is set
        if (!process.argv.find(a => a === '-m=false')) {
          htmlNode = await minify(htmlNode, {
            caseSensitive: true,
            collapseWhitespace: true,
            keepClosingSlash: true,
            minifyCSS: true,
            minifyJS: true,
            minifyURLs: true,
            noNewlinesBeforeTagClose: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeRedundantAttributes: true
          })
        }
        fs.writeFileSync(wrapperFile, htmlNode)
        console.log('[sir] Created HTML version of ' + svelteFilePath)
      } catch (error) {
        console.log('[sir]', error)
      }
      // remove temp file
      fs.unlinkSync(tempFile)
    } else {
      console.log(`[sir] No svelte-file found for node ${node}.`)
    }
  }
}

const removeTempFileIfExists = () => {
  if (fs.existsSync(tempFile)) {
    fs.unlinkSync(tempFile)
  }
}

// remove temp files if node.js will be exited (strg + c)
process.on('SIGTERM', () => {
  removeTempFileIfExists()
  process.exit(0)
})
process.on('SIGINT', () => {
  removeTempFileIfExists()
  process.exit(0)
})

module.exports = build
