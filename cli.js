#!/usr/bin/env node
const build = require('./build-html')

if (process.argv.length > 2 && process.argv[2] !== '-m') {
  build(process.argv[2])
} else {
  build(process.cwd())
}
